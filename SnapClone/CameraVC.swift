//
//  ViewController.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/7/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit
import FirebaseAuth

class CameraVC: AAPLCameraViewController, AAPLCameraVCDelegate {

    @IBOutlet weak var previewView: AAPLPreviewView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    
    override func viewDidLoad() {
        delegate = self
        super._previewView = previewView
        super._previewView.frame = previewView.bounds
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {

//        performSegue(withIdentifier: "goToLogInVC", sender: nil)
        
        // Check if the user is logged in
        guard FIRAuth.auth()?.currentUser != nil else {
            print("User Logged In")
            
            // Load Login VC
            performSegue(withIdentifier: "goToLogInVC", sender: nil)
            return
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func recordButtonPressed(_ sender: Any) {
        toggleMovieRecording()
    }

    @IBAction func changeCameraButtonPressed(_ sender: Any) {
        changeCamera()
    }
    
    func shouldEnableCameraUI(_ enable: Bool) {
        cameraButton.isEnabled = enable
        print("Should enable camera UI: \(enable)")
    }
    
    func shouldEnableRecordUI(_ enable: Bool) {
        recordButton.isEnabled = enable
        print("Should enable record UI: \(enable)")
    }
    
    func recordingHasStarted() {
        print("Recording has started")
    }
    
    func canStartRecording() {
        print("Can Start Recording")
    }
    
}

