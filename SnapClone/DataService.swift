//
//  DataService.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/9/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

let FIR_CHILD_USERS = "users"
let FIR_CHILD_USERS_PROFILE = "profile"

import Foundation
//import Firebase
import FirebaseDatabase

class DataService {
    
    private static let _instance = DataService()
    
    static var instance : DataService {
        return _instance
    }
    
    var mainRef: FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    
    var usersRef: FIRDatabaseReference {
        return mainRef.child(FIR_CHILD_USERS)
    }
    
    func saveUser(uid: String) {
        let profile : Dictionary<String, Any> = [
            "firstName" : "",
            "lastName" : ""
        ]
        
        // If child uid is not there it is automatically created
        mainRef.child(FIR_CHILD_USERS).child(uid).child(FIR_CHILD_USERS_PROFILE).setValue(profile)
    }
    
}
