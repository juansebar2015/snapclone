//
//  RoundTextField.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/8/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable   // So it shows the changes in the interface builder
class RoundTextField: UITextField {
    
    // IBInspectable is going to create a property that we can change in interface builder
    @IBInspectable var cornerRadius: CGFloat = 0 {      // cornerRadius is comming from interface builder and we use it to modify properties
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0      //Cuts anything behind the round edges
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var bgColor : UIColor? {
        didSet {
            backgroundColor = bgColor
        }
    }
    
    @IBInspectable var placeHolderColor : UIColor? {
        didSet {
            
            let rawString = attributedPlaceholder?.string != nil ? attributedPlaceholder!.string : ""
            let string = NSAttributedString(string: rawString, attributes: [NSForegroundColorAttributeName: placeHolderColor!])
            attributedPlaceholder = string
        }
    }
}
