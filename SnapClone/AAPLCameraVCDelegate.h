//
//  AAPLCameraVCDelegate.h
//  SnapClone
//
//  Created by Juan Ramirez on 5/7/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

#ifndef AAPLCameraVCDelegate_h
#define AAPLCameraVCDelegate_h

#import <Foundation/Foundation.h>

//@protocol AAPLCameraVCDelegate <NSObject>
//
//
//
//@end

@protocol AAPLCameraVCDelegate <NSObject>

-(void)shouldEnableRecordUI:(BOOL)enable;
-(void)shouldEnableCameraUI:(BOOL)enable;
-(void)canStartRecording;
-(void)recordingHasStarted;

@end


#endif /* AAPLCameraVCDelegate_h */
