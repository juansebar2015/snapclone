//
//  LogInVC.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/8/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit

class LogInVC: UIViewController {

    @IBOutlet weak var emailTextField: RoundTextField!
    @IBOutlet weak var passwordTextField: RoundTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func logInPressed(_ sender: Any) {
        
        if let email = emailTextField.text , let pass = passwordTextField.text, (email.characters.count > 0 && pass.characters.count > 0) {
            
            // Call the log in service
            AuthService.instance.login(email: email, password: pass, onComplete: { (errMsg, data) in
                
                guard errMsg == nil else {
                    self.displayAlert(alertTitle: "Error Authentication", message: errMsg!)
                    return
                }
                
                self.dismiss(animated: true, completion: nil)
            })
            
        } else {
            displayAlert(alertTitle: "Username and Password Required", message: "You must enter both a username and a password")
        }
        
    }
    
    func displayAlert(alertTitle: String, message: String) {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}
