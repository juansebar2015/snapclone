//
//  UsersVC.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/10/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit
import FirebaseDatabase

class UsersVC: UIViewController {

    fileprivate var users : [User] = [User]()
    fileprivate var selectedUsers : Dictionary<String, User> = Dictionary<String, User>()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        
        // Download the data only ONCE                      // When it recieves data once
        DataService.instance.usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let users = snapshot.value as? Dictionary<String, AnyObject> {
                self.users = []
                
                for (key, value) in users {
                    if let dict = value as? Dictionary<String, AnyObject> {
                        
                        if let profileDict = dict["profile"] as? Dictionary<String, AnyObject> {
                            if let firstName = profileDict["firstName"] as? String {
                                let uid = key
                                let user = User(uid: uid, firstName: firstName)
                                self.users.append(user)
                            }
                        }
                    }
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
}

// TableView Delegate methods
extension UsersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? UserCell {
            cell.setCheckmark(selected: true)
            
            let user = users[indexPath.row]
            let uid = user.uid
            
            selectedUsers[uid] = user
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? UserCell {
            cell.setCheckmark(selected: false)
            
            let uid = users[indexPath.row].uid

            selectedUsers.removeValue(forKey: uid)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as? UserCell {
            
            cell.updateUI(user: users[indexPath.row])
            return cell
            
        } else {
            return UserCell()
        }
    }
}
