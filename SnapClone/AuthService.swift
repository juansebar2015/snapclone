//
//  AuthService.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/9/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import Foundation
import FirebaseAuth

// Simple block we can call at any given time
typealias Completion = (_ errMsg: String?, _ data: AnyObject?) -> Void

class AuthService {
    
    private static let _instance = AuthService()
    
    static var instance : AuthService {
        return _instance
    }
    
    func login(email: String, password: String, onComplete: Completion?) {
        
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                // Handle log in error
                if let errorCode = FIRAuthErrorCode(rawValue: (error?._code)!) {
                    
                    if errorCode == FIRAuthErrorCode.errorCodeUserNotFound {
                        
                        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
                        
                            if error != nil {
                                // Show error to user
                                self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                            } else {
                                // Successfully created new User
                                if user?.uid != nil {
                                    // Save new user to Firebase DB
                                    DataService.instance.saveUser(uid: user!.uid)
                                    
                                    // Sign in 
                                    FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
                                        
                                        if error != nil {
                                            // Show error to user
                                            self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                                        } else {
                                            // Successfully logged in
                                            onComplete?(nil, user)
                                        }
                                    })
                                }
                            }
                            
                        })
                    }
                } else {
                    // Handle all other errors
                    self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                }
            } else {
                // User successfully logged in
                onComplete?(nil, user)
            }
            
        })
    }
    
    
    func handleFirebaseError(error: NSError, onComplete: Completion?) {
        print(error.debugDescription)
        
        if let errorCode = FIRAuthErrorCode(rawValue: error.code) {
            
            switch errorCode {
            case .errorCodeInvalidEmail:
                onComplete?("Invalid email address", nil)
                break
            case .errorCodeWrongPassword:
                onComplete?("Invalid password", nil)
                break
            case .errorCodeEmailAlreadyInUse, .errorCodeAccountExistsWithDifferentCredential:
                onComplete?("Could not create account. Email already in use", nil)
                break
            case .errorCodeWeakPassword:
                onComplete?("Password should be at least 6 characters", nil)
                break
            default:
                onComplete?("There was a problem authenticating. Try again.", nil)
                break
            }
            
        }
    }
}
