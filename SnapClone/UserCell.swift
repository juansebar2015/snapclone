//
//  UserCell.swift
//  SnapClone
//
//  Created by Juan Ramirez on 5/10/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCheckmark(selected: false)
    }
    
    func updateUI(user: User) {
        firstNameLabel.text = user.firstName
    }
    
    func setCheckmark (selected: Bool) {
        let indicatorImage = selected ? #imageLiteral(resourceName: "messageindicatorchecked1") : #imageLiteral(resourceName: "messageindicator1")
        
        self.accessoryView = UIImageView(image: indicatorImage)
        
    }

}
